package net.cnri.util.javascript;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Phaser;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.LogManager;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Engine;
import org.graalvm.polyglot.io.FileSystem;

/**
 * A provider of {@link JavaScriptRunner} objects.
 */
public class JavaScriptEnvironment {
    private final FileSystem fileSystem;
    private final ClassLoader classLoader;
    private final JavaScriptSourceCache javaScriptSourceCache;
    private final SharedModuleSystem sharedModuleSystem;
    private final BoundedObjectPool<JavaScriptRunner> globalPool;
    private final AtomicLong generation = new AtomicLong(1);
    private final ScheduledExecutorService delayedLogExecServ = Executors.newSingleThreadScheduledExecutor();
    private final ConcurrentMap<Long, EngineAndCount> engines = new ConcurrentHashMap<>();
    private final Phaser shutdownPhaser = new Phaser(1);
    private volatile boolean isClosed;

    public JavaScriptEnvironment(RequireLookup requireLookup, ClassLoader classLoader) {
        if (requireLookup == null) this.fileSystem = null;
        else this.fileSystem = new FileSystemFromRequireLookup(requireLookup);
        this.classLoader = classLoader;
        this.sharedModuleSystem = new SharedModuleSystem(requireLookup);
        this.javaScriptSourceCache = new JavaScriptSourceCache();
        this.globalPool = new BoundedObjectPool<JavaScriptRunner>() {
            @Override
            protected JavaScriptRunner create() {
                return createRunner();
            }

            @Override
            protected boolean reset(JavaScriptRunner runner) {
                return resetRunner(runner);
            }

            @Override
            protected Future<?> destroy(JavaScriptRunner runner) {
                return destroyRunner(runner);
            }
        };
    }

    private Engine newEngine() {
        return Engine.newBuilder()
                .allowExperimentalOptions(true)
// Unfortunately this seems to have some extremely sporadic race condition at least in Graal 22.0.0.2
//                .option("js.unhandled-rejections", "warn")
                .option("js.global-property", "true")
                .option("js.json-modules", "true")
                .option("js.import-assertions", "true")
                .option("js.error-cause", "true")
                .logHandler(getRootJulHandler())
                .build();
    }

    private EngineAndCount getCurrentEngineAndCount() {
        long thisGeneration = generation.get();
        EngineAndCount engineAndCount = engines.computeIfPresent(thisGeneration, (gen, oldEngineAndCount) ->
                new EngineAndCount(oldEngineAndCount.generation, oldEngineAndCount.engine, oldEngineAndCount.count + 1));
        if (engineAndCount == null) {
            synchronized (engines) {
                thisGeneration = generation.get();
                engineAndCount = engines.computeIfPresent(thisGeneration, (gen, oldEngineAndCount) ->
                        new EngineAndCount(oldEngineAndCount.generation, oldEngineAndCount.engine, oldEngineAndCount.count + 1));
                if (engineAndCount == null) {
                    @SuppressWarnings("resource")
                    Engine engine = newEngine();
                    engineAndCount = new EngineAndCount(thisGeneration, engine, 1);
                    engines.put(thisGeneration, engineAndCount);
                    closeOlderEngines(thisGeneration);
                }
            }
        }
        return engineAndCount;
    }

    private JavaScriptRunner createRunner() {
        EngineAndCount engineAndCount = getCurrentEngineAndCount();
        shutdownPhaser.register();
        try {
            JavaScriptRunner runner = new JavaScriptRunner(javaScriptSourceCache, sharedModuleSystem, engineAndCount.engine, engineAndCount.generation, delayedLogExecServ, fileSystem, classLoader);
            runner.getEventLoop().submitHoldingExceptions(() -> {
                @SuppressWarnings("resource")
                Context context = runner.getContext();
                context.getPolyglotBindings().putMember("recycleAsync", (Runnable)(() -> recycleAsync(runner)));
            });
            return runner;
        } catch (Exception e) {
            shutdownPhaser.arriveAndDeregister();
            throw e;
        }
    }

    // Note: this happens on the event loop of the runner
    private boolean resetRunner(JavaScriptRunner runner) {
        runner.getEventLoop().clear();
        long contextGeneration = runner.getGeneration();
        return contextGeneration >= generation.get();
    }

    private Future<?> destroyRunner(JavaScriptRunner runner) {
        JavaScriptEventLoop eventLoop = runner.getEventLoop();
        return eventLoop.setImmediate(Thread.getDefaultUncaughtExceptionHandler(), () -> {
            shutdownPhaser.arriveAndDeregister();
            eventLoop.clear();
            @SuppressWarnings("resource")
            Context context = runner.getContext();
            context.leave();
            context.close();
            eventLoop.shutdown();
            long contextGeneration = runner.getGeneration();
            EngineAndCount engineAndCount = engines.compute(contextGeneration, (gen, oldEngineAndCount) -> {
                if (oldEngineAndCount == null) return null;
                if (oldEngineAndCount.count == 1 && contextGeneration < generation.get()) return null;
                return new EngineAndCount(oldEngineAndCount.generation, oldEngineAndCount.engine, oldEngineAndCount.count - 1);
            });
            if (engineAndCount == null) runner.getEngine().close();
        });
    }

    private void closeOlderEngines(long thisGeneration) {
        for (long olderGen : engines.keySet()) {
            if (olderGen >= thisGeneration) continue;
            EngineAndCount olderEngineAndCount = engines.get(olderGen);
            if (olderEngineAndCount == null) continue;
            if (olderEngineAndCount.count > 0) continue;
            if (engines.remove(olderGen, olderEngineAndCount)) {
                olderEngineAndCount.engine.close();
            }
        }
    }

    public void putExtraGlobal(String key, Object obj) {
        javaScriptSourceCache.put(key, obj);
    }

    public void setMaxPoolSize(int maxPoolSize) {
        globalPool.setMaxPoolSize(maxPoolSize);
    }

    /**
     * Clears the cache of compiled scripts, as well as the global pool to eliminate each module cache.
     */
    public void clearCache() {
        sharedModuleSystem.clearCache();
        generation.incrementAndGet();
        globalPool.clear();
    }

    public JavaScriptRunner getRunner(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, Object logger) {
        if (isClosed) throw new IllegalStateException("JavaScriptEnvironment is closed.");
        JavaScriptRunner runner = globalPool.obtain();
        runner.setupLogging(uncaughtExceptionHandler, logger);
        return runner;
    }

    public void recycle(JavaScriptRunner runner) {
        JavaScriptEventLoop eventLoop = runner.getEventLoop();
        eventLoop.submit(() -> {
            if (runner.getAsync()) {
                recycleWhenAllTasksDone(runner);
            } else {
                recycleAsync(runner);
            }
        });
    }

    private void recycleWhenAllTasksDone(JavaScriptRunner runner) {
        JavaScriptEventLoop eventLoop = runner.getEventLoop();
        long delay = eventLoop.getFirstDelay();
        if (delay < 0) {
            recycleAsync(runner);
        } else {
            eventLoop.setTimeout(Thread.getDefaultUncaughtExceptionHandler(), () -> recycleWhenAllTasksDone(runner), delay);
        }
    }

    /**
     * In JavaScript, use Polyglot.import("setAsync")(true) to prevent synchronous recycling.
     * The context will be recycled when all tasks complete.
     * Alternatively, use Polyglot.import("recycleAsync")() to prompt recycling even before scheduled tasks complete.
     */
    private void recycleAsync(JavaScriptRunner runner) {
        runner.setAsync(false);
        globalPool.recycle(runner);
    }

    public void warmUp() {
        recycle(getRunner(null, null));
    }

    public void shutdown() {
        isClosed = true;
        setMaxPoolSize(0);
        globalPool.clear();
        try {
            shutdownPhaser.awaitAdvanceInterruptibly(shutdownPhaser.arriveAndDeregister(), 2, TimeUnit.MINUTES);
        } catch (Exception e) {
            if (e instanceof InterruptedException) Thread.currentThread().interrupt();
            // ignore
        }
        for (EngineAndCount engineAndCount : engines.values()) {
            engineAndCount.engine.close(true);
        }
        delayedLogExecServ.shutdown();
    }

    private static class EngineAndCount {
        long generation;
        Engine engine;
        long count;

        EngineAndCount(long generation, Engine engine, long count) {
            this.generation = generation;
            this.engine = engine;
            this.count = count;
        }
    }

    private static Handler getRootJulHandler() {
        Handler[] rootHandlers = LogManager.getLogManager().getLogger("").getHandlers();
        Handler rootHandler;
        if (rootHandlers == null || rootHandlers.length == 0) {
            rootHandler = new ConsoleHandler();
        } else {
            rootHandler = rootHandlers[rootHandlers.length - 1];
        }
        return rootHandler;
    }
}
