package net.cnri.util.javascript;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class LoggingOutputStream extends OutputStream {
    private final ByteArrayOutputStream buffer = new ByteArrayOutputStream(1024) {
        @Override
        public synchronized void reset() {
            super.reset();
            if (buf.length > 2048) buf = new byte[2048];
        }
    };

    private final Consumer<String> log;
    private final ScheduledExecutorService execServ;
    private Future<?> future;

    public LoggingOutputStream(Consumer<String> log, ScheduledExecutorService execServ) {
        this.log = log;
        this.execServ = execServ;
    }

    @Override
    public void write(int b) throws IOException {
        byte bbyte = (byte)(b & 0xFF);
        synchronized (buffer) {
            buffer.write(bbyte);
            if (buffer.size() > 1024) {
                writeBuffer();
                return;
            }
            if (bbyte == '\n') {
                writeBuffer();
                return;
            }
            ensureLog();
        }
    }

    @Override
    public void write(byte[] b) throws IOException {
        write(b, 0, b.length);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        if (b == null) {
            throw new NullPointerException();
        } else if ((off < 0) || (off > b.length) || (len < 0) ||
                   ((off + len) > b.length) || ((off + len) < 0)) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return;
        }
        synchronized (buffer) {
            buffer.write(b, off, len);
            if (buffer.size() > 1024) {
                writeBuffer();
                return;
            }
            for (int i = off + len - 1; i >= off; i--) {
                if (b[i] == '\n') {
                    writeBuffer();
                    return;
                }
            }
            ensureLog();
        }
    }

    private void ensureLog() {
        synchronized (buffer) {
            if (future != null) return;
            future = execServ.schedule(this::writeBuffer, 500, TimeUnit.MILLISECONDS);
        }
    }

    @Override
    public void flush() throws IOException {
        synchronized (buffer) {
            if (buffer.size() > 0) writeBuffer();
        }
    }

    private void writeBuffer() {
        synchronized (buffer) {
            if (future != null) {
                future.cancel(false);
                future = null;
            }
            if (buffer.size() == 0) return;
            // Note: using system-default character encoding intentionally
            String s = new String(buffer.toByteArray());
            if (s.endsWith("\n")) s = s.substring(0, s.length() - 1);
            log.accept(s);
            buffer.reset();
        }
    }
}
