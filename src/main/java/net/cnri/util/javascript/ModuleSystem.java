package net.cnri.util.javascript;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.PolyglotException;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.proxy.ProxyExecutable;
import org.graalvm.polyglot.proxy.ProxyObject;

// All the methods of this class and its constructor are used only from JavaScriptRunner and are called inside the event loop
public class ModuleSystem {
    private final JavaScriptRunner javaScriptRunner;
    private final Context context;
    private final SharedModuleSystem shared;
    private final Map<String, Object> moduleCache;
    private final Value newModuleBuilder;
    private final Value globalRequire;

    public ModuleSystem(JavaScriptSourceCache javaScriptSourceCache, JavaScriptRunner javaScriptRunner, SharedModuleSystem shared) {
        this.javaScriptRunner = javaScriptRunner;
        this.context = javaScriptRunner.getContext();
        this.shared = shared;
        this.moduleCache = new HashMap<>();
        this.newModuleBuilder = javaScriptSourceCache.newModuleBuilder(this.context);
        this.globalRequire = javaScriptSourceCache.globalRequire(this.context).execute((ProxyExecutable) this::nativeRequire);
    }

    private static final byte[] MODULE_PREFIX = "(function (exports, require, module, __filename, __dirname) {(function(){".getBytes(StandardCharsets.UTF_8);
    private static final byte[] MODULE_SUFFIX = "\n}).bind(exports)()})".getBytes(StandardCharsets.UTF_8);

    private static String asString(Path path) {
        return path.toString().replace(File.separator, "/");
    }

    private static String dirname(String s) {
        int lastSlash = s.lastIndexOf('/');
        if (lastSlash == -1) return ".";
        if (lastSlash == 0) return "/";
        return s.substring(0, lastSlash);
    }

    private boolean moduleExists(String id) {
        if (moduleCache.containsKey(id)) return true;
        if (shared.exists(id)) return true;
        return false;
    }

    // the passing of Value parent is to allow for loading package.json
    public String resolve(Value parent, String id) {
        String parentFilename = (parent == null || parent.isNull()) ? "/." : parent.getMember("filename").asString();
        String cachedResolution = shared.getCachedModuleResolution(parentFilename, id);
        if (cachedResolution != null) return cachedResolution;
        String res = resolveUncached(parent, parentFilename, id);
        if (res == null) return null;
        shared.cacheModuleResolution(parentFilename, id, res);
        return res;
    }

    private String resolveUncached(Value parent, String parentFilename, String id) {
        String resolvedCoreModule = resolveFile(id);
        if (resolvedCoreModule != null) return resolvedCoreModule;
        try {
            Path parentFilepath = Paths.get(parentFilename);
            if (id.startsWith("./") || id.startsWith("../") || id.startsWith("/")) {
                String absoluteId = asString(parentFilepath.resolveSibling(id).normalize());
                String resolvedFile = resolveFile(absoluteId);
                if (resolvedFile != null) return resolvedFile;
                resolvedFile = resolveDirectory(parent, absoluteId);
                if (resolvedFile != null) return resolvedFile;
            } else {
                while (true) {
                    parentFilepath = parentFilepath.getParent();
                    if (parentFilepath == null) break;
                    if (parentFilepath.getFileName() != null && parentFilepath.getFileName().toString().equals("node_modules")) continue;
                    String absoluteId = asString(parentFilepath.resolve("node_modules").resolve(id).normalize());
                    String resolvedFile = resolveFile(absoluteId);
                    if (resolvedFile != null) return resolvedFile;
                    resolvedFile = resolveDirectory(parent, absoluteId);
                    if (resolvedFile != null) return resolvedFile;
                }
            }
            return null;
        } catch (PolyglotException e) {
            throw e;
        } catch (RuntimeException e) {
            throw javaScriptRunner.reasonToException(e);
        }
    }

    private String resolveFile(String absoluteId) {
        if (moduleExists(absoluteId)) return absoluteId;
        if (moduleExists(absoluteId + ".js")) return absoluteId + ".js";
        if (moduleExists(absoluteId + ".json")) return absoluteId + ".json";
        return null;
    }

    private String resolveDirectory(Value parent, String absoluteId) {
        String main = getMainFromPackageJson(parent, absoluteId);
        if (main != null) {
            String resolvedFile = resolveFile(asString(Paths.get(absoluteId).resolve(main).normalize()));
            if (resolvedFile != null) return resolvedFile;
        } else {
            if (moduleExists(absoluteId + "/index.js")) {
                return absoluteId + "/index.js";
            }
            if (moduleExists(absoluteId + "/index.json")) {
                return absoluteId + "/index.json";
            }
        }
        return null;
    }

    private String getMainFromPackageJson(Value parent, String absoluteId) {
        Value packageJsonModule = getModule(parent, absoluteId + "/package.json");
        if (packageJsonModule != null) {
            Value exports = packageJsonModule.getMember("exports");
            Value mainObj = exports.getMember("main");
            if (mainObj.isString()) {
                return mainObj.asString();
            }
        }
        return null;
    }

    private Value getModule(Value parent, String filename) {
        {
            Object module = moduleCache.get(filename);
            if (module != null) return context.asValue(module);
        }
        {
            Source source = shared.getSource(filename);
            if (source != null) {
                return getModuleFromSource(parent, filename, source);
            }
        }
        {
            try (InputStream moduleContent = shared.getContent(filename)) {
                if (moduleContent != null) {
                    return requireModuleFromContent(parent, filename, moduleContent);
                }
            } catch (IOException e) {
                throw javaScriptRunner.reasonToException(e);
            }
        }
        return null;
    }

    public static String readFullyAsString(InputStream in) throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        int r;
        byte[] buf = new byte[8192];
        while ((r = in.read(buf)) > 0) {
            bout.write(buf, 0 ,r);
        }
        return new String(bout.toByteArray(), StandardCharsets.UTF_8);
    }

    public Value requireModuleFromContent(Value parent, String filename, InputStream moduleContent) {
        try {
            if (filename.endsWith(".json")) {
                String moduleContentString = readFullyAsString(moduleContent);
                return getModuleFromJson(parent, filename, moduleContentString);
            } else {
                Source source;
                try (InputStream wrappedModule = new SequenceInputStream(Collections.enumeration(Arrays.asList(new ByteArrayInputStream(MODULE_PREFIX), moduleContent, new ByteArrayInputStream(MODULE_SUFFIX))))) {
                    source = shared.cache(filename, wrappedModule);
                }
                return getModuleFromSource(parent, filename, source);
            }
        } catch (PolyglotException e) {
            throw e;
        } catch (IOException | RuntimeException e) {
            throw javaScriptRunner.reasonToException(e);
        }
    }

    private Value getModuleFromJson(Value parent, String filename, String moduleContentString) {
        Value module = newModule(parent, filename);
        module.putMember("exports", javaScriptRunner.getSyncMethods().jsonParse(moduleContentString));
        // since it can't be recursive, ok to cache after parsing
        moduleCache.put(filename, module);
        module.putMember("loaded", Boolean.TRUE);
        return module;
    }

    private Value getModuleFromSource(Value parent, String filename, Source source) {
        Value module = newModule(parent, filename);
        moduleCache.put(filename, module);
        try {
            Value moduleWrapper = context.eval(source);
            moduleWrapper.execute(module.getMember("exports"), module.getMember("require"), module, filename, dirname(filename));
        } catch (Exception e) {
            moduleCache.remove(filename);
            throw e;
        }
        module.putMember("loaded", Boolean.TRUE);
        return module;
    }

    private Value newModule(Value parent, String filename, ProxyObject moduleCacheProxy, ProxyExecutable requireModule, ProxyExecutable resolve) {
        return newModuleBuilder.execute(parent, filename, moduleCacheProxy, requireModule, resolve);
    }

    private Value newModule(Value parent, String filename) {
        return newModule(parent, filename,
            ProxyObject.fromMap(moduleCache),
            this::requireModuleProxy,
            this::resolveProxy);
    }

    private Value requireModuleProxy(Value... arguments) {
        if (arguments == null || arguments.length != 2 || arguments[1] == null || !arguments[1].isString()) {
            throw new AssertionError("Unexpected arguments in ModuleSystem.requireModuleProxy");
        }
        return requireModule(arguments[0], arguments[1].asString());
    }

    private String resolveProxy(Value... arguments) {
        if (arguments == null || arguments.length != 2 || arguments[1] == null || !arguments[1].isString()) {
            throw new AssertionError("Unexpected arguments in ModuleSystem.resolveProxy");
        }
        return resolve(arguments[0], arguments[1].asString());
    }

    public Value require(Value parent, String id) {
        Value module = requireModule(parent, id);
        if (module == null) return null;
        return module.getMember("exports");
    }

    public Value nativeRequire(Value... arguments) {
        if (arguments == null || arguments.length != 1 || arguments[0] == null || !arguments[0].isString()) {
            throw new AssertionError("Unexpected arguments in ModuleSystem.nativeRequire");
        }
        String id = arguments[0].asString();
        return requireModule(null, id);
    }

    public Value globalRequire(Value... arguments) {
        if (arguments == null || arguments.length != 1 || arguments[0] == null || !arguments[0].isString()) {
            throw new AssertionError("Unexpected arguments in ModuleSystem.globalRequire");
        }
        String id = arguments[0].asString();
        return this.globalRequire.execute(id);
    }

    public Value globalRequire(String id) {
        return this.globalRequire.execute(id);
    }

    public Value requireModule(Value parent, String id) {
        String parentFilename = (parent == null || parent.isNull()) ? "/." : parent.getMember("filename").asString();
        return requireModule(parent, parentFilename, id);
    }

    public Value requireModule(Value parent, String parentFilename, String id) {
        String cachedResolution = shared.getCachedModuleResolution(parentFilename, id);
        if (cachedResolution != null) {
            return requireModuleFile(parent, cachedResolution);
        }
        Value module = requireModuleUncached(parent, parentFilename, id);
        if (module == null || module.isNull()) return null;
        String filename = module.getMember("filename").asString();
        shared.cacheModuleResolution(parentFilename, id, filename);
        return module;
    }

    private Value requireModuleUncached(Value parent, String parentFilename, String id) {
        Value resolvedCoreModule = requireModuleFile(parent, id);
        if (resolvedCoreModule != null) return resolvedCoreModule;
        try {
            Path parentFilepath = Paths.get(parentFilename);
            if (id.startsWith("./") || id.startsWith("../") || id.startsWith("/")) {
                String absoluteId = asString(parentFilepath.resolveSibling(id).normalize());
                Value resolvedModule = requireModuleFile(parent, absoluteId);
                if (resolvedModule != null) return resolvedModule;
                resolvedModule = requireModuleDirectory(parent, absoluteId);
                if (resolvedModule != null) return resolvedModule;
            } else {
                while (true) {
                    parentFilepath = parentFilepath.getParent();
                    if (parentFilepath == null) break;
                    if (parentFilepath.getFileName() != null && parentFilepath.getFileName().toString().equals("node_modules")) continue;
                    String absoluteId = asString(parentFilepath.resolve("node_modules").resolve(id).normalize());
                    Value resolvedModule = requireModuleFile(parent, absoluteId);
                    if (resolvedModule != null) return resolvedModule;
                    resolvedModule = requireModuleDirectory(parent, absoluteId);
                    if (resolvedModule != null) return resolvedModule;
                }
            }
            return null;
        } catch (PolyglotException e) {
            throw e;
        } catch (Exception e) {
            throw javaScriptRunner.reasonToException(e);
        }
    }

    private Value requireModuleFile(Value parent, String absoluteId) {
        Value module = getModule(parent, absoluteId);
        if (module != null) return module;
        module = getModule(parent, absoluteId + ".js");
        if (module != null) return module;
        module = getModule(parent, absoluteId + ".json");
        if (module != null) return module;
        return null;
    }

    private Value requireModuleDirectory(Value parent, String absoluteId) {
        String main = getMainFromPackageJson(parent, absoluteId);
        if (main != null) {
            Value resolvedModule = requireModuleFile(parent, asString(Paths.get(absoluteId).resolve(main).normalize()));
            if (resolvedModule != null) return resolvedModule;
        } else {
            Value module = getModule(parent, absoluteId + "/index.js");
            if (module != null) return module;
            module = getModule(parent, absoluteId + "/index.json");
            if (module != null) return module;
        }
        return null;
    }

}
