package net.cnri.util.javascript;

import java.io.Reader;
import java.lang.reflect.Method;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Engine;
import org.graalvm.polyglot.PolyglotException;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;
import org.graalvm.polyglot.io.FileSystem;
import org.graalvm.polyglot.proxy.ProxyExecutable;

public class JavaScriptRunner {
    private final Engine engine;
    private final Context context;
    private final JavaScriptEventLoop eventLoop;
    private final long generation;
    private final AtomicBoolean async;
    private final SyncMethods syncMethods = new SyncMethods();
    private ModuleSystem moduleSystem;
    private Object logger;
    private Value jsonValue;
    private Value bindValue;
    private Value throwerValue;
    private Value isUndefinedValue;
    private Value importValue;
    private Value promisifyValue;

    public JavaScriptRunner(JavaScriptSourceCache javaScriptSourceCache, SharedModuleSystem sharedModuleSystem, Engine engine, long generation, ScheduledExecutorService delayedLogExecServ, FileSystem fileSystem, ClassLoader classLoader) {
        this.generation = generation;
        this.engine = engine;
        this.eventLoop = new JavaScriptEventLoop();
        this.async = new AtomicBoolean();
        @SuppressWarnings("resource")
        LoggingOutputStream out = new LoggingOutputStream(s -> log(false, s), delayedLogExecServ);
        @SuppressWarnings("resource")
        LoggingOutputStream err = new LoggingOutputStream(s -> log(true, s), delayedLogExecServ);
        Context.Builder builder = Context.newBuilder("js")
                .engine(engine)
                .allowAllAccess(true)
                .out(out)
                .err(err);
        if (classLoader != null) {
            builder.hostClassLoader(classLoader);
        }
        if (fileSystem != null) builder = builder.fileSystem(fileSystem);
        this.context = builder.build();
        this.eventLoop.submitHoldingExceptions(() -> {
            context.enter();
            context.getPolyglotBindings().putMember("eventLoop", eventLoop);
            context.getPolyglotBindings().putMember("stdout", out);
            context.getPolyglotBindings().putMember("stderr", err);
            context.getPolyglotBindings().putMember("setAsync", (Consumer<Boolean>)(b -> async.set(b)));
            javaScriptSourceCache.initializeContext(context);
            this.jsonValue = javaScriptSourceCache.json(this.context);
            this.bindValue = javaScriptSourceCache.bind(this.context);
            this.throwerValue = javaScriptSourceCache.thrower(this.context);
            this.isUndefinedValue = javaScriptSourceCache.isUndefined(this.context);
            this.importValue = javaScriptSourceCache.importFunction(this.context);
            this.promisifyValue = javaScriptSourceCache.promisify(this.context);
            this.moduleSystem = new ModuleSystem(javaScriptSourceCache, this, sharedModuleSystem);
            context.getBindings("js").putMember("require", (ProxyExecutable) moduleSystem::globalRequire);
        });
    }

    private void log(boolean isError, String s) {
        try {
            if (logger != null) {
                String functionName = isError ? "error" : "info";
                Method method = logger.getClass().getMethod(functionName, String.class);
                method.invoke(logger, s);
                return;
            }
        } catch (Exception e) {
            // unable to get logger; ignore
        }
        if (isError) {
            System.err.println(s);
        } else {
            System.out.println(s);
        }
    }

    public void setupLogging(Thread.UncaughtExceptionHandler uncaughtExceptionHandler, Object logger) {
        this.logger = logger;
        this.eventLoop.submitHoldingExceptions(() -> {
            context.getPolyglotBindings().putMember("logger", logger);
            Thread.UncaughtExceptionHandler ueh = uncaughtExceptionHandler;
            if (uncaughtExceptionHandler == null) {
                ueh = (t, e) -> loggerWarn(logger, t, e);
            }
            context.getPolyglotBindings().putMember("uncaughtExceptionHandler", ueh);
        });
    }

    Engine getEngine() {
        return engine;
    }

    Context getContext() {
        return context;
    }

    JavaScriptEventLoop getEventLoop() {
        return eventLoop;
    }

    long getGeneration() {
        return generation;
    }

    boolean getAsync() {
        return async.get();
    }

    void setAsync(boolean b) {
        async.set(b);
    }

    // methods which must be called inside the event loop
    public SyncMethods getSyncMethods() {
        return syncMethods;
    }

    public class SyncMethods {

        public Value asValue(Object obj) {
            return context.asValue(obj);
        }

        public Value jsonParse(String json) {
            Value res = jsonValue.invokeMember("parse", json);
            if (res == null || res.isNull()) return null;
            return res;
        }

        public String jsonStringify(Object obj) {
            Value res = jsonValue.invokeMember("stringify", obj);
            if (res == null || res.isNull()) return null;
            return res.asString();
        }

        public boolean isUndefined(Value value) {
            Value res = isUndefinedValue.execute(value);
            return res.asBoolean();
        }

        public boolean isTruthy(Value obj) {
            if (obj == null || obj.isNull()) return false;
            if (obj.isBoolean()) return obj.asBoolean();
            if (obj.isString()) return !obj.asString().isEmpty();
            if (obj.isNumber()) {
                double num = obj.asDouble();
                if (Double.isNaN(num)) return false;
                if (num == 0.0) return false;
                return true;
            }
            return true;
        }

        public Value requireById(String id) {
            return moduleSystem.globalRequire(id);
        }

        // returns promise
        public Value importById(String id) {
            return importValue.execute(id);
        }

        public void throwValue(Value reason) {
            throwerValue.execute(reason);
        }

        public Value promisify(ThrowingSupplier<?> resolutionSupplier) {
            return promisifyValue.execute(withUncheckedExceptions(resolutionSupplier));
        }

        public Value promiseThen(Value promise, ThrowingFunction<Value, Object> onFulfilled) {
            return promise.invokeMember("then", withUncheckedExceptions(onFulfilled));
        }

        public Value promiseThen(Value promise, ThrowingFunction<Value, Object> onFulfilled, ThrowingFunction<Value, Object> onRejected) {
            return promise.invokeMember("then", withUncheckedExceptions(onFulfilled), withUncheckedExceptions(onRejected));
        }

        public Value promiseThen(Value promise, ThrowingFunction<Value, Object> onFulfilled, ThrowingConsumer<Value> onRejected) {
            return promise.invokeMember("then", withUncheckedExceptions(onFulfilled), withUncheckedExceptions(onRejected));
        }

        public Value promiseThen(Value promise, ThrowingConsumer<Value> onFulfilled) {
            return promise.invokeMember("then", withUncheckedExceptions(onFulfilled));
        }

        public Value promiseThen(Value promise, ThrowingConsumer<Value> onFulfilled, ThrowingFunction<Value, Object> onRejected) {
            return promise.invokeMember("then", withUncheckedExceptions(onFulfilled), withUncheckedExceptions(onRejected));
        }

        public Value promiseThen(Value promise, ThrowingConsumer<Value> onFulfilled, ThrowingConsumer<Value> onRejected) {
            return promise.invokeMember("then", withUncheckedExceptions(onFulfilled), withUncheckedExceptions(onRejected));
        }
    }

    public Value eval(String source) throws InterruptedException {
        return eval(source, false);
    }

    public Value eval(String source, boolean isEsm) throws InterruptedException {
        Source.Builder sourceBuilder = Source.newBuilder("js", source, "Unnamed");
        if (isEsm) sourceBuilder.mimeType("application/javascript+module");
        Source sourceObj = sourceBuilder.buildLiteral();
        return eventLoop.submitAndGet(() -> context.eval(sourceObj));
    }

    public Value eval(Reader source) throws InterruptedException {
        return eval(source, false);
    }

    public Value eval(Reader source, boolean isEsm) throws InterruptedException {
        Source.Builder sourceBuilder = Source.newBuilder("js", source, "Unnamed");
        if (isEsm) sourceBuilder.mimeType("application/javascript+module");
        Source sourceObj = sourceBuilder.buildLiteral();
        return eventLoop.submitAndGet(() -> context.eval(sourceObj));
    }

    public Future<?> submit(Runnable runnable) {
        return eventLoop.submit(runnable);
    }

    public <T> Future<T> submit(Callable<T> callable) {
        return eventLoop.submit(callable);
    }

    public void submitAndGet(Runnable runnable) throws InterruptedException {
        eventLoop.submitAndGet(runnable);
    }

    public <T> T submitAndGet(Callable<T> callable) throws InterruptedException {
        return eventLoop.submitAndGet(callable);
    }

    @FunctionalInterface
    public interface ThrowingFunction<T, R> {
        R apply(T t) throws Exception;
    }

    @FunctionalInterface
    public interface ThrowingConsumer<T> {
        void accept(T t) throws Exception;
    }

    @FunctionalInterface
    public interface ThrowingSupplier<T> {
        T get() throws Exception;
    }

    private ProxyExecutable withUncheckedExceptions(ThrowingFunction<Value, Object> fn) {
        return values -> {
            try {
                if (values == null || values.length == 0) return fn.apply(null);
                else return fn.apply(values[0]);
            } catch (RuntimeException | Error e) {
                throw e;
            } catch (Throwable th) {
                throw new CompletionException(th);
            }
        };
    }

    private ProxyExecutable withUncheckedExceptions(ThrowingConsumer<Value> consumer) {
        return values -> {
            try {
                if (values == null || values.length == 0) consumer.accept(null);
                else consumer.accept(values[0]);
                return null;
            } catch (RuntimeException | Error e) {
                throw e;
            } catch (Throwable th) {
                throw new CompletionException(th);
            }
        };
    }

    private ProxyExecutable withUncheckedExceptions(ThrowingSupplier<?> supplier) {
        return values -> {
            try {
                return supplier.get();
            } catch (RuntimeException | Error e) {
                throw e;
            } catch (Throwable th) {
                throw new CompletionException(th);
            }
        };
    }

    public <T> CompletableFuture<T> convertToJavaPromise(ThrowingSupplier<Value> maybePromiseSupplier, ThrowingFunction<Value, T> onFulfilled, ThrowingFunction<PolyglotException, T> onRejected) {
        CompletableFuture<T> javaPromise = new CompletableFuture<>();
        // The event loop holds onto exceptions during startup; this allows
        // promise-based code to eventually see those exceptions
        eventLoop.submitProcessingHeldException(heldException -> {
            try {
                if (heldException != null) throw heldException;
                Value maybePromise = maybePromiseSupplier.get();
                if (maybePromise == null) {
                    javaPromise.complete(onFulfilled.apply(null));
                    return;
                }
                if (!(maybePromise.hasMember("then"))) {
                    javaPromise.complete(onFulfilled.apply(maybePromise));
                    return;
                }
                if (maybePromise.canInvokeMember("then")) {
                    maybePromise.invokeMember("then",
                        withUncheckedExceptions(res -> { javaPromise.complete(onFulfilled.apply(context.asValue(res))); }),
                        withUncheckedExceptions(reason -> {
                            handleExceptionInPromise(javaPromise, onRejected, reason);
                        }));
                    return;
                }
                Value then = maybePromise.getMember("then");
                if (then == null || !then.canExecute()) {
                    javaPromise.complete(onFulfilled.apply(maybePromise));
                    return;
                }
                bindValue.execute(then, maybePromise).execute(
                    withUncheckedExceptions(res -> { javaPromise.complete(onFulfilled.apply(context.asValue(res))); }),
                    withUncheckedExceptions(reason -> {
                        handleExceptionInPromise(javaPromise, onRejected, reason);
                    }));
            } catch (Throwable t) {
                handleExceptionInPromise(javaPromise, onRejected, t);
            }
        });
        return javaPromise;
    }

    private <T> void handleExceptionInPromise(CompletableFuture<T> javaPromise, ThrowingFunction<PolyglotException, T> onRejected, Object reason) {
        PolyglotException ex = reasonToException(reason);
        if (onRejected == null) javaPromise.completeExceptionally(ex);
        else try {
            javaPromise.complete(onRejected.apply(ex));
        } catch (Throwable th) {
            PolyglotException ex2 = reasonToException(th);
            ex2.addSuppressed(ex);
            javaPromise.completeExceptionally(ex2);
        }
    }

    public <T> T awaitPromise(ThrowingSupplier<Value> maybePromiseSupplier, ThrowingFunction<Value, T> onFulfilled, ThrowingFunction<PolyglotException, T> onRejected) throws InterruptedException {
        try {
            return convertToJavaPromise(maybePromiseSupplier, onFulfilled, onRejected).get();
        } catch (ExecutionException e) {
            throw rethrowExecutionException(e);
        }
    }

    public Value awaitPromise(Value maybePromise) throws InterruptedException {
        try {
            return convertToJavaPromise(() -> maybePromise, x -> x, null).get();
        } catch (ExecutionException e) {
            throw rethrowExecutionException(e);
        }
    }

    public static RuntimeException rethrowExecutionException(ExecutionException e) throws InterruptedException {
        return JavaScriptEventLoop.rethrowExecutionException(e);
    }

    private static void loggerWarn(Object logger, Thread t, Throwable e) {
        String message = "Uncaught exception";
        if (t != null) message += " on " + t.getName();
        if (e instanceof PolyglotException) {
            Value ee = ((PolyglotException) e).getGuestObject();
            if (ee != null && ee.hasMembers()) {
                if (ee.hasMember("name")) message += ": " + ee.getMember("name");
                if (ee.hasMember("message")) message += ": " + ee.getMember("message");
                if (ee.hasMember("cause") && ee.getMember("cause").hasMembers()) {
                    if (ee.getMember("cause").hasMember("name")) message += ": " + ee.getMember("cause").getMember("name");
                    if (ee.getMember("cause").hasMember("message")) message += ": " + ee.getMember("cause").getMember("message");
                }
            }
        }
        if (logger != null) {
            try {
                Method method = logger.getClass().getMethod("warn", String.class, Throwable.class);
                method.invoke(logger, message, e);
                return;
            } catch (Exception ex) {
                // fall-through
            }
        }
        java.lang.System.err.println(message);
        e.printStackTrace();
    }

    // done in eventLoop
    PolyglotException reasonToException(Object reason) {
        if (reason instanceof PolyglotException) return (PolyglotException)reason;
        if (reason instanceof WrappedPolyglotException) return ((WrappedPolyglotException)reason).getRootCause();
        try {
            throwerValue.execute(reason);
            return null;
        } catch (PolyglotException pe) {
            if (reason instanceof Value && ((Value) reason).isHostObject() && ((Value) reason).asHostObject() instanceof Throwable) {
                reason = ((Value) reason).asHostObject();
            }
            if (reason instanceof Throwable) {
                if (pe == reason) return pe;
                if (pe.getCause() == null) {
                    try {
                        pe.initCause((Throwable)reason);
                    } catch (Exception ex) {
                        pe.addSuppressed((Throwable)reason);
                    }
                } else {
                    pe.addSuppressed((Throwable)reason);
                }
            }
            return pe;
        }
    }
}
