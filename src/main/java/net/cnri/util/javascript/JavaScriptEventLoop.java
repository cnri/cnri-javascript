package net.cnri.util.javascript;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import org.graalvm.polyglot.PolyglotException;
import org.graalvm.polyglot.Value;

import net.cnri.util.javascript.JavaScriptRunner.ThrowingConsumer;

public class JavaScriptEventLoop {
    private static final ThreadLocal<JavaScriptEventLoop> threadLocalJavaScriptEventLoop = new ThreadLocal<>();
    private static final AtomicInteger threadNumber = new AtomicInteger(1);

    private final ScheduledThreadPoolExecutor execServ;
    private Throwable heldException; // exception during setup; if set, the runner is broken beyond repair

    public JavaScriptEventLoop() {
        execServ = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(1, r -> new Thread(r, "JavaScript-event-loop-" + threadNumber.getAndIncrement()));
        execServ.setExecuteExistingDelayedTasksAfterShutdownPolicy(false);
        execServ.setRemoveOnCancelPolicy(true);
        submitHoldingExceptions(() -> threadLocalJavaScriptEventLoop.set(this));
    }

    public <T> Future<T> submit(Callable<T> c) {
        if (threadLocalJavaScriptEventLoop.get() == this) {
            CompletableFuture<T> future = new CompletableFuture<>();
            try {
                if (heldException != null) {
                    throw asException(heldException);
                }
                future.complete(c.call());
            } catch (Throwable t) {
                future.completeExceptionally(t);
            }
            return future;
        }
        return execServ.submit(() -> {
            if (heldException != null) {
                throw asException(heldException);
            }
            return c.call();
        });
    }

    private static Exception asException(Throwable t) {
        if (t instanceof Exception) return (Exception)t;
        if (t instanceof Error) throw (Error)t;
        throw new RuntimeException(t);
    }

    public void submitHoldingExceptions(Callable<?> c) {
        submit(() -> {
            if (heldException != null) return null;
            try {
                return c.call();
            } catch (Throwable t) {
                heldException = t;
                return null;
            }
        });
    }

    public void submitProcessingHeldException(ThrowingConsumer<Throwable> consumer) {
        submit(() -> {
            consumer.accept(heldException);
            return null;
        });
    }

    public <T> T submitAndGet(Callable<T> c) throws InterruptedException {
        try {
            return submit(c).get();
        } catch (ExecutionException e) {
            throw rethrowExecutionException(e);
        }
    }

    public Future<?> submit(Runnable r) {
        return submit(Executors.callable(r));
    }

    public void submitHoldingExceptions(Runnable r) {
        submitHoldingExceptions(Executors.callable(r));
    }

    public void submitAndGet(Runnable r) throws InterruptedException {
        submitAndGet(Executors.callable(r));
    }

    /**
     * Ensures that no tasks remain queued, but do not terminate
     */
    public void clear() {
        execServ.getQueue().clear();
    }

    public long getFirstDelay() {
        Runnable runnable = execServ.getQueue().peek();
        if (runnable == null) return -1; // returns -1 only when the queue is empty
        if (!(runnable instanceof ScheduledFuture<?>)) return 0;
        ScheduledFuture<?> future = (ScheduledFuture<?>) runnable;
        long delay = future.getDelay(TimeUnit.MILLISECONDS);
        if (delay <= 0) return 0;
        return delay;
    }

    public void shutdown() {
        execServ.shutdown();
    }

    public Future<?> setImmediate(Object ueh, Value fn, Object... args) {
        return execServ.submit(handleExceptions(ueh, () -> fn.execute(args)));
    }

    public Future<?> setImmediate(Object ueh, Runnable runnable) {
        return execServ.submit(handleExceptions(ueh, runnable));
    }

    public Future<?> setTimeout(Object ueh, Value fn, long delay, Object... args) {
        return setTimeout(ueh, () -> fn.execute(args), delay);
    }

    public Future<?> setTimeout(Object ueh, Runnable runnable, long delay) {
        return execServ.schedule(handleExceptions(ueh, runnable), delay, TimeUnit.MILLISECONDS);
    }

    public Future<?> setInterval(Object ueh, Value fn, long delay, Object... args) {
        return execServ.scheduleWithFixedDelay(handleExceptions(ueh, () -> fn.execute(args)), delay, delay, TimeUnit.MILLISECONDS);
    }

    private Runnable handleExceptions(Object ueh, Runnable runnable) {
        return () -> {
            try {
                runnable.run();
            } catch (Throwable t) {
                if (ueh instanceof Thread.UncaughtExceptionHandler) {
                    ((Thread.UncaughtExceptionHandler)ueh).uncaughtException(Thread.currentThread(), t);
                } else if (ueh instanceof Value && ((Value)ueh).canExecute()) {
                    ((Value)ueh).execute(Thread.currentThread(), t);
                } else {
                    t.printStackTrace();
                }
                throw t;
            }
        };
    }

    public static RuntimeException rethrowExecutionException(ExecutionException e) throws InterruptedException {
        Throwable cause = e.getCause();
        while (cause instanceof ExecutionException || cause instanceof CompletionException) {
            cause = cause.getCause();
        }
        if (cause instanceof PolyglotException) throw new WrappedPolyglotException((PolyglotException)cause, e);
        if (cause instanceof InterruptedException) throw withCause(new InterruptedException(cause.getMessage()), e);
        if (cause instanceof Error) throw new Error(cause.getMessage(), e);
        if (cause instanceof Exception) throw new CompletionException(cause.getMessage(), e);
        throw new CompletionException(e);
    }

    public static <T extends Throwable> T withCause(T exception, Throwable cause) {
        exception.initCause(cause);
        return exception;
    }
}
