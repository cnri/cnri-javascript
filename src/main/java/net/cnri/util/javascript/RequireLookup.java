package net.cnri.util.javascript;
import java.io.InputStream;

public interface RequireLookup {
    /**
     * Returns true if the "file" exists and is a file (not a directory), otherwise false.
     * @param filename name of the file
     * @return true if the "file" exists and is a file (not a directory), otherwise false.
     */
    boolean exists(String filename);

    /**
     * Returns true if the "file" is ESM (or has an ESM variant); false if CJS-only; null if should be determined by file extension or package.json.
     * @param filename name of the file
     * @return true if the "file" is ESM (or has an ESM variant); false if CJS-only; null if should be determined by file extension or package.json
     */
    Boolean isEsm(String filename);

    /**
     * Returns true if the JSON is an object with property "type" having value "module".
     * @param json the JSON (from a package.json)
     * @return true if the JSON is an object with property "type" having value "module"
     */
    boolean isTypeModule(String json);

    /**
     * Returns an {@code InputStream} with the content of the file if it exists, otherwise null.
     *
     * @param filename name of the file
     * @param isEsm true if the file is being imported (rather than required); this allows both esm and cjs versions of modules
     * @return an {@code InputStream} with the content of the file if it exists, otherwise null.
     */
    InputStream getContent(String filename, boolean isEsm);
}
