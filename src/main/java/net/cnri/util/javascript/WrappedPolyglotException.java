package net.cnri.util.javascript;

import java.util.concurrent.ExecutionException;

import org.graalvm.polyglot.PolyglotException;

public class WrappedPolyglotException extends RuntimeException {
    private final PolyglotException rootCause;

    public WrappedPolyglotException(PolyglotException rootCause, ExecutionException cause) {
        super(rootCause.getMessage(), cause);
        this.rootCause = rootCause;
    }

    public PolyglotException getRootCause() {
        return rootCause;
    }
}
