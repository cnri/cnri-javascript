package net.cnri.util.javascript;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.graalvm.polyglot.Source;

public class SharedModuleSystem {
    private final RequireLookup requireLookup;
    private final ConcurrentMap<String, Source> sourceCache = new ConcurrentHashMap<>();
    private final Map<String, Map<String, String>> moduleResolveCache = new ConcurrentHashMap<>();

    public SharedModuleSystem(RequireLookup requireLookup) {
        this.requireLookup = requireLookup;
    }

    private static String asString(Path path) {
        return path.toString().replace(File.separator, "/");
    }

    public void clearCache() {
        moduleResolveCache.clear();
        sourceCache.clear();
    }

    public boolean exists(String id) {
        if (sourceCache.containsKey(id)) return true;
        if (requireLookup != null && requireLookup.exists(id)) return true;
        return false;
    }

    public Source getSource(String id) {
        return sourceCache.get(id);
    }

    public Source cache(String name, InputStream script) throws IOException {
        try (InputStreamReader isr = new InputStreamReader(script, StandardCharsets.UTF_8)) {
            Source source = Source.newBuilder("js", isr, name)
                .cached(true)
                .mimeType("application/javascript")
                .uri(getUri(name))
                .build();
            sourceCache.putIfAbsent(name, source);
            return source;
        }
    }

    private URI getUri(String name) {
        if (name.startsWith("/")) return URI.create("file://" + encodeForUrl(name));
        return URI.create("core:" + encodeForUrl(name));
    }

    private String encodeForUrl(String name) {
        try {
            String res = URLEncoder.encode(name, "UTF-8");
            res = res.replace("%2F", "/");
            return res;
        } catch (UnsupportedEncodingException e) {
            throw new AssertionError(e);
        }
    }

    public String getCachedModuleResolution(String parentFilename, String id) {
        if (parentFilename == null) parentFilename = "/.";
        // for absolute ids, resolution is not specific to parent
        if (id.startsWith("./") || id.startsWith("../") || id.startsWith("/")) {
            id = asString(Paths.get(parentFilename).resolveSibling(id).normalize());
            parentFilename = "/.";
        }
        String key = getKeyFromParent(parentFilename);
        Map<String, String> subMap = moduleResolveCache.get(key);
        if (subMap == null) return null;
        return subMap.get(id);
    }

    public void cacheModuleResolution(String parentFilename, String id, String resolution) {
        if (resolution == null) return;
        if (parentFilename == null) parentFilename = "/.";
        // for absolute ids, resolution is not specific to parent
        if (id.startsWith("./") || id.startsWith("../") || id.startsWith("/")) {
            id = asString(Paths.get(parentFilename).resolveSibling(id).normalize());
            parentFilename = "/.";
        }
        String key = getKeyFromParent(parentFilename);
        Map<String, String> subMap = moduleResolveCache.computeIfAbsent(key, s -> new ConcurrentHashMap<>());
        subMap.put(id, resolution);
    }

    private String getKeyFromParent(String parentFilename) {
        // otherwise, only the container matters
        Path grandparentPath = Paths.get(parentFilename).getParent();
        if (grandparentPath == null) return "/";
        else return asString(grandparentPath.normalize());
    }

    public InputStream getContent(String id) {
        if (requireLookup == null) return null;
        return requireLookup.getContent(id, false);
    }
}
