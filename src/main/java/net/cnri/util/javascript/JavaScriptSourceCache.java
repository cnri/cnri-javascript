package net.cnri.util.javascript;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Source;
import org.graalvm.polyglot.Value;

// Information shared among all contexts: internal source cache, extra globals
// Also helper methods for using internal JS code; these are called from JavaScriptRunner
// and always inside the event loop.
public class JavaScriptSourceCache {
    private final ConcurrentMap<String, Source> internalSourceCache;
    private final Map<String, Object> extraGlobals = new ConcurrentHashMap<>();

    public JavaScriptSourceCache() {
        this.internalSourceCache = new ConcurrentHashMap<>();
        initialize();
    }

    private void initialize() {
        try {
            initResource("timers.js");
            initResource("process.js");
            initResource("module.js");
            initResource("globalRequire.js");
            initString("bind", "(function (fn, x) { return fn.bind(x); })");
            initString("thrower", "(function (reason) { throw reason; })");
            initString("JSON", "JSON");
            initString("import", "(function (id) { return import(id); })");
            initString("isUndefined", "(function (value) { return value === undefined; })");
            initString("promisify", "(function (valueSupplier) { return new Promise(function (resolve) { resolve(valueSupplier()); }); })");
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }

    private URI getUri(String path) {
        return URI.create("file:///cnri/_internal/" + path);
    }

    private void initString(String name, String script) {
        Source source = Source.newBuilder("js", script, name)
                .cached(true)
                .internal(true)
                .mimeType("application/javascript")
                .uri(getUri(name))
                .buildLiteral();
        internalSourceCache.putIfAbsent(name, source);
    }

    private void initResource(String name) throws IOException {
        try (InputStreamReader isr = new InputStreamReader(JavaScriptRunner.class.getResourceAsStream(name), StandardCharsets.UTF_8)) {
            Source source = Source.newBuilder("js", isr, name)
                    .cached(true)
                    .internal(true)
                    .mimeType("application/javascript")
                    .uri(getUri(name))
                    .build();
            internalSourceCache.putIfAbsent(name, source);
        }
    }

    public void put(String key, Object obj) {
        extraGlobals.put(key, obj);
    }

    public void initializeContext(Context context) {
        context.eval(internalSourceCache.get("timers.js"));
        context.eval(internalSourceCache.get("process.js"));
        for (Map.Entry<String, Object> entry : extraGlobals.entrySet()) {
            context.getBindings("js").putMember(entry.getKey(), entry.getValue());
        }
    }

    Value json(Context context) {
        return context.eval(internalSourceCache.get("JSON"));
    }

    Value bind(Context context) {
        return context.eval(internalSourceCache.get("bind"));
    }

    Value thrower(Context context) {
        return context.eval(internalSourceCache.get("thrower"));
    }

    Value isUndefined(Context context) {
        return context.eval(internalSourceCache.get("isUndefined"));
    }

    Value importFunction(Context context) {
        return context.eval(internalSourceCache.get("import"));
    }

    Value newModuleBuilder(Context context) {
        return context.eval(internalSourceCache.get("module.js"));
    }

    Value globalRequire(Context context) {
        return context.eval(internalSourceCache.get("globalRequire.js"));
    }

    Value promisify(Context context) {
        return context.eval(internalSourceCache.get("promisify"));
    }
}
