package net.cnri.util.javascript;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.AccessMode;
import java.nio.file.CopyOption;
import java.nio.file.DirectoryStream;
import java.nio.file.DirectoryStream.Filter;
import java.nio.file.LinkOption;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.graalvm.polyglot.io.FileSystem;

public class FileSystemFromRequireLookup implements FileSystem {

    private final static Path root = Paths.get("/");

    private final RequireLookup requireLookup;

    public FileSystemFromRequireLookup(RequireLookup requireLookup) {
        this.requireLookup = requireLookup;
    }

    private boolean isCoreModule(String id) {
        return !id.isEmpty() && !id.contains("/");
    }

    private static String asString(Path path) {
        return path.toString().replace(File.separator, "/");
    }

    @Override
    public Path parsePath(String path) {
        return Paths.get(path);
    }

    @Override
    public Path toAbsolutePath(Path path) {
        String id = asString(path);
        if (id.isEmpty()) {
            return root;
        } else if (isCoreModule(id)) {
            if (requireLookup.exists(id)) return path;
        }
        return root.resolve(path).normalize();
    }

    @Override
    public Path toRealPath(Path path, LinkOption... linkOptions) throws IOException {
        return toAbsolutePath(path);
    }

    @Override
    public String getSeparator() {
        return "/";
    }

    @Override
    public void checkAccess(Path path, Set<? extends AccessMode> modes, LinkOption... linkOptions) throws IOException {
        String id = asString(path);
        if (!requireLookup.exists(id)) throw new FileNotFoundException("Cannot find module '" + id + "'");
    }

    @Override
    public Map<String, Object> readAttributes(Path path, String attributes, LinkOption... options) throws IOException {
        String id = asString(path);
        return Collections.singletonMap("isRegularFile", requireLookup.exists(id));
    }

    @SuppressWarnings("resource")
    @Override
    public SeekableByteChannel newByteChannel(Path path, Set<? extends OpenOption> options, FileAttribute<?>... attrs) throws IOException {
        String id = asString(path);
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        try (InputStream in = getEsmContent(id, path)) {
            if (in == null) throw new FileNotFoundException("Cannot find module '" + id + "'");
            byte[] buf = new byte[8192];
            int r;
            while ((r = in.read(buf)) > 0) {
                bout.write(buf, 0, r);
            }
        }
        byte[] bytes = bout.toByteArray();
        long size = bytes.length;
        ReadableByteChannel channel = Channels.newChannel(new ByteArrayInputStream(bytes));
        return new SeekableByteChannel() {

            @Override
            public boolean isOpen() {
                return channel.isOpen();
            }

            @Override
            public void close() throws IOException {
                channel.close();
            }

            @Override
            public int write(ByteBuffer src) throws IOException {
                throw new UnsupportedOperationException();
            }

            @Override
            public SeekableByteChannel truncate(long sizeParam) throws IOException {
                throw new UnsupportedOperationException();
            }

            @Override
            public long size() throws IOException {
                return size;
            }

            @Override
            public int read(ByteBuffer dst) throws IOException {
                return channel.read(dst);
            }

            @Override
            public SeekableByteChannel position(long newPosition) throws IOException {
                throw new UnsupportedOperationException();
            }

            @Override
            public long position() throws IOException {
                throw new UnsupportedOperationException();
            }
        };
    }

    private boolean isEsm(String id, Path path) {
        Boolean isEsm = requireLookup.isEsm(id);
        if (isEsm == Boolean.TRUE) return true;
        if (isEsm == Boolean.FALSE) return false;
        String lowerId = id.toLowerCase(Locale.ROOT);
        if (lowerId.endsWith(".json")) return true;
        if (lowerId.endsWith(".mjs")) return true;
        if (lowerId.endsWith(".cjs")) return false;
        String nearestPackageJsonId = getNearestPackageJsonId(path);
        if (nearestPackageJsonId == null) return false;
        try (InputStream in = requireLookup.getContent(nearestPackageJsonId, false)) {
            if (in == null) return false;
            String nearestPackageJsonString = ModuleSystem.readFullyAsString(in);
            return requireLookup.isTypeModule(nearestPackageJsonString);
        } catch (IOException e) {
            return false;
        }
    }

    private String getNearestPackageJsonId(Path path) {
        while (path != null) {
            String packageJsonId = asString(path.resolveSibling("package.json"));
            if (requireLookup.exists(packageJsonId)) return packageJsonId;
            path = path.getParent();
        }
        return null;
    }

    private InputStream getEsmContent(String id, Path path) {
        if (isEsm(id, path)) {
            return requireLookup.getContent(id, true);
        } else {
            String string = "export default require('" + escapeString(id) + "');";
            return new ByteArrayInputStream(string.getBytes(StandardCharsets.UTF_8));
        }
    }

    private static String escapeString(String s) {
        StringBuilder sb = new StringBuilder(s);
        for (int i = 1; i < sb.length() - 1; i++) {
            char ch = sb.charAt(i);
            if (ch == '"') {
                sb.replace(i, i+1, "\\\"");
                i++;
            } else if (ch == '\\') {
                sb.replace(i, i+1, "\\\\");
                i++;
            } else if (ch >= 0x20) {
                continue;
            } else if (ch == '\b') {
                sb.replace(i, i+1, "\\b");
                i++;
            } else if (ch == '\f') {
                sb.replace(i, i+1, "\\f");
                i++;
            } else if (ch == '\n') {
                sb.replace(i, i+1, "\\n");
                i++;
            } else if (ch == '\r') {
                sb.replace(i, i+1, "\\r");
                i++;
            } else if (ch == '\t') {
                sb.replace(i, i+1, "\\t");
                i++;
            } else {
                sb.replace(i, i+1, String.format("\\u%04x", (int)ch));
                i += 5;
            }
        }
        return sb.toString();
    }

    @Override
    public Path parsePath(URI uri) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void createDirectory(Path dir, FileAttribute<?>... attrs) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Path path) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public DirectoryStream<Path> newDirectoryStream(Path dir, Filter<? super Path> filter) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setAttribute(Path path, String attribute, Object value, LinkOption... options) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void copy(Path source, Path target, CopyOption... options) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void move(Path source, Path target, CopyOption... options) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void createLink(Path link, Path existing) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void createSymbolicLink(Path link, Path target, FileAttribute<?>... attrs) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public Path readSymbolicLink(Path link) throws IOException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setCurrentWorkingDirectory(Path currentWorkingDirectory) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Path getTempDirectory() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isSameFile(Path path1, Path path2, LinkOption... options) throws IOException {
        throw new UnsupportedOperationException();
    }
}
