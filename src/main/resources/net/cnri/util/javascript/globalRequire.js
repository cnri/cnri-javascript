(function (nativeRequire) {
    return (function (id) {
        var childModule = nativeRequire(id);
        if (!childModule) {
            var error = new Error("Cannot find module '" + id + "'");
            error.code = 'MODULE_NOT_FOUND';
            throw error;
        }
        return childModule.exports;
    });
})