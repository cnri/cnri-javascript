(function (parent, filename, moduleCache, requireModule, resolveFilename) {
    var module = {
        parent: parent,
        filename: filename,
        exports: {},
        children: [],
        id: filename,
        loaded: false
    };
    var require = function (id) {
        if (typeof id !== 'string') throw { name:'Error', message:'The "id" argument must be of type string', code: 'ERR_INVALID_ARG_TYPE' };
        var childModule = requireModule(module, id);
        if (!childModule) throw { name:'Error', message:"Cannot find module '" + id + "'", code: 'MODULE_NOT_FOUND' };
        return childModule.exports;
    };
    require.main = parent ? parent.require.main : module;
    require.resolve = function (id) {
        if (typeof id !== 'string') throw { name:'Error', message:'The "id" argument must be of type string', code: 'ERR_INVALID_ARG_TYPE' };
        var res = resolveFilename(module, id);
        if (!res) throw { name:'Error', message:"Cannot find module '" + id + "'", code: 'MODULE_NOT_FOUND' };
        return res;
    };
    require.cache = moduleCache;
    module.require = require;
    if (parent) parent.children.push(module);
    return module;
})